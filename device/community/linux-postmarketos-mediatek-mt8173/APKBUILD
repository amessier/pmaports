# Maintainer: Adam Thiede <me@adamthiede.com>
# Co-Maintainer: Jenneron <jenneron@protonmail.com>
pkgname=linux-postmarketos-mediatek-mt8173
pkgver=6.7.9
pkgrel=0
pkgdesc="Mainline kernel fork for Mediatek MT8173 devices"
arch="aarch64"
_carch="arm64"
_flavor="${pkgname#linux-}"
url="https://kernel.org"
license="GPL-2.0-only"
options="!strip !check !tracedeps
	pmb:cross-native
	pmb:kconfigcheck-community
"
makedepends="
	bash
	bison
	findutils
	flex
	installkernel
	openssl-dev
	perl
	gmp-dev
	mpc1-dev
	mpfr-dev
	xz
"

# Source
_config="config-$_flavor.$arch"
case $pkgver in
	*.*.*)	_kernver=${pkgver%.0};;
	*.*)	_kernver=$pkgver;;
esac
source="
	https://cdn.kernel.org/pub/linux/kernel/v6.x/linux-${pkgver//_/-}.tar.xz
	$_config
	fix-mmc1-speed.patch
	fix-mmc-order.patch
	fix-spi-nor-max-frequency.patch
"
builddir="$srcdir/linux-${_kernver//_/-}"

prepare() {
	default_prepare
	cp "$srcdir/config-$_flavor.$CARCH" .config
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-postmarketOS"
}

package() {
	mkdir -p "$pkgdir"/boot
	make zinstall modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_PATH="$pkgdir"/boot \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_MOD_STRIP=1 \
		INSTALL_DTBS_PATH="$pkgdir/boot/dtbs"
	rm -f "$pkgdir"/lib/modules/*/build "$pkgdir"/lib/modules/*/source

	install -D "$builddir"/include/config/kernel.release \
		"$pkgdir"/usr/share/kernel/$_flavor/kernel.release
}

sha512sums="
8f660d1322c427c15fe9168e560740d30d7a3fa6f2cd2f9563c921bde8714b7aa8f81d3ad3cf5b08a75e86440d5b782237073c8afbe6b06bcdba02ff09103cb9  linux-6.7.9.tar.xz
bd2d1fa16d004b11c90b2c8498f883907def3845d3ee06603da07bad31e527350ad6f3c70b3c37c253e66542e435ac31a2c901f6a3b5de1d8fd0825c02397e92  config-postmarketos-mediatek-mt8173.aarch64
4b499c1fbf53631cffd6fa7299643dc744e0e2187f71804664b02f05296162b42e3f76aa0d8c688cecb43a8bcd41ec92991c98287951292260237b828dcca710  fix-mmc1-speed.patch
c86f8dfc32165a32381d45a4c8b6811ebf43f01b5d8a48fbe227cf5084cfefe24b32264c1c150cb49115db4759a2d21ad48b37dcaac78367d226e9cc2a5ba849  fix-mmc-order.patch
caf48ac0f77661153ee94c7de4931baca135a69a97e93f01ad8f276b4a9944e077d7214c117450943cca07990c773661f79718cb0c2ff7c5789c93d37afb26de  fix-spi-nor-max-frequency.patch
"
